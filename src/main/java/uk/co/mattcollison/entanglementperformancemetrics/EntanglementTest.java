package uk.co.mattcollison.entanglementperformancemetrics;

import com.entanglementgraph.graph.data.Edge;
import com.entanglementgraph.graph.data.Node;
import com.entanglementgraph.revlog.commands.EdgeModification;
import com.entanglementgraph.revlog.commands.GraphOperation;
import com.entanglementgraph.revlog.commands.MergePolicy;
import com.entanglementgraph.revlog.commands.NodeModification;
import com.entanglementgraph.util.GraphConnection;
import com.entanglementgraph.util.GraphConnectionFactory;
import com.entanglementgraph.util.GraphConnectionFactoryException;
import com.entanglementgraph.util.TxnUtils;
import com.scalesinformatics.util.UidGenerator;
import com.mongodb.ServerAddress;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Hello world!
 *
 */
public class EntanglementTest {

    private static GraphConnection graphConn;

//    public static void main(String[] args) {
//        helloWorld();
//
//    }

    //simple hello world graph
    public static void helloWorld() {

        //start mongodb on local machine 
        try {
            String command = "C://mongodb/mongodb-win32-x86_64-2008plus-2.4.8/bin/mongod.exe";
            Process p = Runtime.getRuntime().exec(command);
            System.out.println("mongodb has been initialised on the current machine. ");
        } catch (IOException ex) {
            System.out.println("Error executing mongod.exe command");
            ex.printStackTrace();
        }

        //connect to the database         
        try {
            GraphConnectionFactory.registerNamedPool("local-cluster",
                    new ServerAddress("localhost"));
            GraphConnectionFactory factory = new GraphConnectionFactory(
                    EntanglementTest.class.getClassLoader(), "local-cluster", "databasename");
            //use connection to specify graphname and version 
            graphConn = factory.connect("graphName", "branchName");
            System.out.println("Sucesfully connected to graph");
        } catch (GraphConnectionFactoryException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        //create two nodes and an edge connecting them 
        try {

            List<GraphOperation> graphOps = new LinkedList<GraphOperation>();

            Node node1 = new Node();
            node1.getKeys().addNames("hello");
            node1.getKeys().setType("type");
//            node1.getKeys().addUids(UidGenerator.generateUid());
            node1.getKeys().addUid("hello");
            graphOps.add(NodeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, node1));

            Node node2 = new Node();
            node2.getKeys().addNames("world");
            node2.getKeys().setType("type");
            node2.getKeys().addUid(UidGenerator.generateUid());
            node2.getKeys().addUid("world");
            graphOps.add(NodeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, node2));

            Edge edge1 = new Edge();
            edge1.getKeys().addNames("Relation");
            edge1.getKeys().setType("type");
            edge1.getKeys().addUids(UidGenerator.generateUid());
            edge1.setFrom(node1.getKeys());
            edge1.setTo(node2.getKeys());
            graphOps.add(EdgeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, edge1));

            TxnUtils.submitAsTxn(graphConn, graphOps);

        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}