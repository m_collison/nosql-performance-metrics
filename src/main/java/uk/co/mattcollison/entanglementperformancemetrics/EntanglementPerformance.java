 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.entanglementperformancemetrics;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class EntanglementPerformance implements PerformanceTasks {

    private int ordersOfMagnitude;
    private List<Double> buildtimeset = new ArrayList<Double>();
    private List<Double> querytimeset = new ArrayList<Double>();
    private List<Integer> buildset = new ArrayList<Integer>();
    private boolean fullyconnected;
    private int replicates;

    public EntanglementPerformance(int replicates, int ordersOfMagnitude, boolean fullyconnected) {

        this.replicates = replicates;
        this.ordersOfMagnitude = ordersOfMagnitude;
        this.fullyconnected = fullyconnected;
    }

    public List<Double> buildGraph() {

        //start mongodb on local machine 
        try {
            String command;
            if (System.getProperty("user.name").equals("a6034850")) {
                command = "D://mongo/mongodb-win32-x86_64-2.4.1/bin/mongod.exe --dbpath D://mongo/data";
            } else if (System.getProperty("user.name").equals("Matt2")) {
                command = "C://mongodb/mongodb-win32-x86_64-2008plus-2.4.8/bin/mongod.exe";
            } else {
                command = null;
                System.out.println("Machine not supported");
            }
            Process p = Runtime.getRuntime().exec(command);
            System.out.println("mongodb has been initialised on the current machine. ");
        } catch (IOException ex) {
            System.out.println("Error executing mongod.exe command");
            ex.printStackTrace();
        }

        //create dummy graph to preinitialise everything that will be used 
        //might want to consider doing this multiple times so java bytes are compiled to machine code 
        buildEntanglementGraph(-2, false);

        for (int l = 0; l < replicates; l++) {
            //create graph sizes for each order of magnitude
            for (int i = 0; i < ordersOfMagnitude; i++) {
                for (int j = 1; j < 10; j++) {
                    buildset.add(((int) Math.pow(10, i)) * j);
                }
            }

            //randomise build order
            Collections.shuffle(buildset);

            for (int i = 0; i < buildset.size(); i++) {
                PrintToFile.addElement(String.valueOf(buildset.get(i)));
                buildtimeset.add(buildEntanglementGraph(buildset.get(i), fullyconnected));
            }
            buildset.clear();
        }
        return buildtimeset;
    }

    public List<Double> queryGraph() {
        //insert code to query graph here
        return querytimeset;
    }

    public void performanceCharts() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private double buildEntanglementGraph(int graphsize, boolean fullyconnected) {

        EntanglementGraphHandler entgh = new EntanglementGraphHandler(graphsize, fullyconnected);
        //do garbage collection before each build so this is less likely to affect the build time
        System.gc();

        long starttime = System.currentTimeMillis();

        entgh.createDB();

        long endtime = System.currentTimeMillis();

        System.out.println(endtime - starttime);
        PrintToFile.addElement(String.valueOf(endtime - starttime) + "\n");
        return (endtime - starttime);
    }
}