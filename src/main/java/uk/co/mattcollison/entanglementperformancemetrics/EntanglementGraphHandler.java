 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.entanglementperformancemetrics;

import com.entanglementgraph.graph.data.Edge;
import com.entanglementgraph.graph.data.EntityKeys;
import com.entanglementgraph.graph.data.Node;
import com.entanglementgraph.revlog.commands.EdgeModification;
import com.entanglementgraph.revlog.commands.GraphOperation;
import com.entanglementgraph.revlog.commands.MergePolicy;
import com.entanglementgraph.revlog.commands.NodeModification;
import com.entanglementgraph.util.GraphConnection;
import com.entanglementgraph.util.GraphConnectionFactory;
import com.entanglementgraph.util.GraphConnectionFactoryException;
import com.entanglementgraph.util.TxnUtils;
import com.scalesinformatics.util.UidGenerator;
import com.mongodb.ServerAddress;
import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class EntanglementGraphHandler {

    private String clustername = "local-cluster";
    private String hostname = "localhost";
    private String databasename = "entanglementPerformance";
    private String graphname = "graphname";
    private String branchname = "master";
    private boolean fullyconnected;
    private int graphsize;
    private GraphConnection graphConn;
    private Map<Integer, EntityKeys> nodekeys = new HashMap<Integer, EntityKeys>();

    public EntanglementGraphHandler(int graphsize, boolean fullyconnected) {
        databasename = databasename + "_" + graphsize;
        this.graphsize = Math.abs(graphsize);
        this.fullyconnected = fullyconnected;
    }

    public EntanglementGraphHandler(int graphsize) {
        databasename = databasename + "_" + graphsize;
        this.graphsize = graphsize;
    }

    public EntanglementGraphHandler() {
    }

    public void createDB() {

        //connect to the database         
        try {
            GraphConnectionFactory.registerNamedPool(clustername,
                    new ServerAddress(hostname));
            GraphConnectionFactory factory = new GraphConnectionFactory(
                    EntanglementTest.class.getClassLoader(), clustername, databasename);
            //use connection to specify graphname and version 
            graphConn = factory.connect(graphname, branchname);
            System.out.println("Sucesfully connected to graph");
        } catch (GraphConnectionFactoryException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            List<GraphOperation> graphOps = new LinkedList<GraphOperation>();

            Node reference = new Node();
            reference.getKeys().addNames("reference");
            reference.getKeys().setType("type");
            reference.getKeys().addUids(UidGenerator.generateUid());
            graphOps.add(NodeModification.create(graphConn,
                    MergePolicy.APPEND_NEW__LEAVE_EXISTING, reference));

            for (int i = 0; i < graphsize; i++) {

                Node node = new Node();
                node.getKeys().addNames("node" + i);
                node.getKeys().setType("type");
                node.getKeys().addUids(UidGenerator.generateUid());
                nodekeys.put(i, node.getKeys());
//                node.getKeys().addUid("Unique ID");
                graphOps.add(NodeModification.create(graphConn,
                        MergePolicy.APPEND_NEW__LEAVE_EXISTING, node));
                
                Edge edge = new Edge();
                edge.getKeys().addNames("Relation");
                edge.getKeys().setType("type");
                edge.getKeys().addUids(UidGenerator.generateUid());
                edge.setFrom(node.getKeys());
                edge.setTo(reference.getKeys());
                graphOps.add(EdgeModification.create(graphConn,
                        MergePolicy.APPEND_NEW__LEAVE_EXISTING, edge));

                if (fullyconnected) {
                    for (int j = 1; j < i; j++) {
                        Edge edge1 = new Edge();
                        edge1.getKeys().addNames("Relation" + i + "_" + j);
                        edge1.getKeys().setType("type");
                        edge1.getKeys().addUids(UidGenerator.generateUid());
                        edge1.setFrom(node.getKeys());
                        edge1.setTo(nodekeys.get(j));
                        graphOps.add(EdgeModification.create(graphConn,
                                MergePolicy.APPEND_NEW__LEAVE_EXISTING, edge1));
                    }
                }
            }

            // Single line begin/submit/commit
            TxnUtils.submitAsTxn(graphConn, graphOps);

//            //use this when transaction doesn't fit into memory
//            // Same as this...
//            String txnID = TxnUtils.beginNewTransaction(graphConn);
//
//            int partId = 0;
//            TxnUtils.submitTxnPart(graphConn, txnID, partId, graphOps);
//            TxnUtils.commitTransaction(graphConn, txnID);

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public String getClustername() {
        return clustername;
    }

    public String getHostname() {
        return hostname;
    }

    public String getDatabasename() {
        return databasename;
    }

    public String getGraphname() {
        return graphname;
    }

    public String getBranchname() {
        return branchname;
    }

    public GraphConnection getGraphConn() {
        return graphConn;
    }
}
