/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.entanglementperformancemetrics;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class Neo4JPerformance implements PerformanceTasks {

    private int ordersOfMagnitude;
    private List<Double> buildtimeset = new ArrayList<Double>();
    private List<Double> querytimeset = new ArrayList<Double>();
    private List<Integer> buildset = new ArrayList<Integer>();
    private boolean fullyconnected;
    private int replicates;

    public Neo4JPerformance(int replicates, int ordersOfMagnitude, boolean fullyconnected) {
        this.replicates = replicates;
        this.ordersOfMagnitude = ordersOfMagnitude;
        this.fullyconnected = fullyconnected;
    }

    public List<Double> buildGraph() {
        
        //create dummy graph to preinitialise everything that will be used 
        //might want to consider doing this multiple times so java bytes are compiled to machine code 
        buildNeoGraph(1, -2, true);

        for (int l = 0; l < replicates; l++) {
            //create graph sizes for each order of magnitude
            for (int i = 0; i < ordersOfMagnitude; i++) {
                for (int j = 1; j < 10; j++) {
                    buildset.add(((int) Math.pow(10, i)) * j);
                }
            }

            //randomise build order
            Collections.shuffle(buildset);

            for (int i = 0; i < buildset.size(); i++) {
                PrintToFile.addElement(String.valueOf(buildset.get(i)));
                
                buildtimeset.add(buildNeoGraph(l, buildset.get(i), fullyconnected));
            }
            buildset.clear();
        }

        return buildtimeset;
    }

    public List<Double> queryGraph() {
        //insert code to query graph here
        return querytimeset;
    }

    public void performanceCharts() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private double buildNeoGraph(int rep, int graphsize, boolean fullyconnected) {

        Neo4JGraphHandler neogh = new Neo4JGraphHandler(rep, graphsize, fullyconnected);
        //do garbage collection before each build so this is less likely to affect the build time
        System.gc();

        long starttime = System.currentTimeMillis();

        neogh.createDB();

        long endtime = System.currentTimeMillis();
        
        System.out.println(endtime - starttime);
        PrintToFile.addElement(String.valueOf(endtime - starttime) + "\n");
        return (endtime - starttime);
    }
}