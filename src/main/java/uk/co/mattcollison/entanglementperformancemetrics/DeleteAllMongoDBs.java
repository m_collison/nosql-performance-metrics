 /*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.entanglementperformancemetrics;

import com.mongodb.MongoClient;
import com.mongodb.DB;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class DeleteAllMongoDBs {
    
    private List<String> dbs = new ArrayList<String>();
    
//    public static void main(String[] args){
//        DeleteAllMongoDBs del = new DeleteAllMongoDBs();
//        del.deleteDBs();
//    }
    
    public void deleteDBs(){

        //connect to the database         
        EntanglementGraphHandler ent = new EntanglementGraphHandler();
        try {
            MongoClient mongoclient = new MongoClient(ent.getHostname());
            dbs = mongoclient.getDatabaseNames();
            for (int i=0;i<dbs.size();i++){
                DB db = mongoclient.getDB(dbs.get(i));
                db.dropDatabase();
            }
            System.out.println("Sucesfully deleted all mongo databases.");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}