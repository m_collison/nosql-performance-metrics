package uk.co.mattcollison.entanglementperformancemetrics;

/**
 *
 *
 */
public class App {

    public static void main(String[] args) {

        int replicates = Integer.parseInt(args[0]);
        int orders = Integer.parseInt(args[1]);
        boolean connected = Boolean.parseBoolean(args[2]);
        
        //initialise outputfile 
        PrintToFile.initialiseOutputFile("buildtimes_" + replicates + "_" + 
                orders + "_" + connected + ".csv");
        
        //neo4J build performance tests with single relationship
        PrintToFile.addLine();
        PrintToFile.addElement("NEO4J");
        PrintToFile.addLine();
        Neo4JPerformance neoPerform = new Neo4JPerformance(replicates, orders, connected);
        neoPerform.buildGraph();

        //entanglement build performance tests with single relationship
        PrintToFile.addLine();
        PrintToFile.addElement("ENTANGLEMENT");
        PrintToFile.addLine();
        EntanglementPerformance entPerform = new EntanglementPerformance(replicates, orders, connected);
        entPerform.buildGraph();

        //delete all mongodb databases on local host and close output file 
        DeleteAllMongoDBs del = new DeleteAllMongoDBs();
        del.deleteDBs();
        PrintToFile.finaliseOutputFile();
    }
}