/*
 * Copyright(c) 2014 Matthew Collison. 
 */
package uk.co.mattcollison.entanglementperformancemetrics;

import java.util.ArrayList;
import java.util.List;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.Transaction;
import org.neo4j.graphdb.index.Index;
import org.neo4j.kernel.EmbeddedGraphDatabase;

/**
 *
 * @author Matthew Collison
 * http://www.ncl.ac.uk/computing/people/student/m.g.collison
 *
 */
public class Neo4JGraphHandler {

    //database properties
    private String DB_PATH = "D://neo4j-performance";
    private final String USERNAME_KEY = "username";
//    private final String NODE_SIZE = "node size";
    private GraphDatabaseService graphDb;
    private Index<Node> nodeIndex;
    List<Node> nodeList = new ArrayList<Node>();
    private int graphsize;
    private boolean fullyconnected;
    
    public Neo4JGraphHandler(int rep, int graphsize, boolean fullyconnected) {
        this.graphsize = Math.abs(graphsize);
        if (fullyconnected) {
            DB_PATH = DB_PATH + "_" + rep + "_" + graphsize + "_fullyconnected";
        } else {
            DB_PATH = DB_PATH + "_" + rep + "_" + graphsize;
        }
        this.fullyconnected = fullyconnected;
    }
    
    public Neo4JGraphHandler(int graphsize) {
        DB_PATH = DB_PATH + "_" + graphsize;
        this.graphsize = graphsize;
    }
    
    private enum RelTypes implements RelationshipType {
        
        RELATIONSHIP1
    }
    
    public void createDB() {

        // startDb
        graphDb = new EmbeddedGraphDatabase(DB_PATH);
        nodeIndex = graphDb.index().forNodes("nodes");
        registerShutdownHook();

        // populateDB
        Transaction tx = graphDb.beginTx();
        try {
            
            Node usersReferenceNode = createAndIndexUser("refNode");
            graphDb.getReferenceNode().createRelationshipTo(
                    usersReferenceNode, RelTypes.RELATIONSHIP1);
            
            for (int i = 0; i < graphsize; i++) {
                //create node
                Node node = createAndIndexUser("no" + i);
                node.createRelationshipTo(usersReferenceNode,
                        RelTypes.RELATIONSHIP1);
                nodeList.add(node);
                
                if (fullyconnected) {
                    //connect node to all other nodes
                    for (int j = 0; j < i; j++) {
//                        Node nodeTo = nodeIndex.get(USERNAME_KEY, "no" + j).getSingle();
//                        node.createRelationshipTo(nodeTo,
//                                RelTypes.RELATIONSHIP1);
                        node.createRelationshipTo(nodeList.get(j),
                                RelTypes.RELATIONSHIP1);
                    }
                }
            }
            tx.success();
        } finally {
            tx.finish();
        }
//        System.out.println("Shutting down database ...");
        shutdown();
    }
    
    private void shutdown() {
        graphDb.shutdown();
    }

//    private String idToUserName(int id) {
//        return "user" + id + "@neo4j.org";
//    }
    private Node createAndIndexUser(String username) {
        Node node = graphDb.createNode();
        node.setProperty(USERNAME_KEY, username);
        nodeIndex.add(node, USERNAME_KEY, username);
        return node;
    }
    
    private void registerShutdownHook() {
        // Registers a shutdown hook for the Neo4j and index service instances
        // so that it shuts down nicely when the VM exits (even if you
        // "Ctrl-C" the running example before it's completed)
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                shutdown();
            }
        });
    }
}
